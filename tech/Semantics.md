Some interesting links and notes about Semantics Technologies.

See also the page on [Data](Data).

## Ontologies

- [BBC ontologies](http://www.bbc.co.uk/ontologies).
- [FOAF](http://xmlns.com/foaf/spec/)

## OWL

**OWL 1**

- OWL [Overview](http://w3.org/TR/owl-features/): introduction, language feature
  list
- OWL [Guide](http://w3.org/TR/owl-guide/): usage examples, includes a
  [glossary](http://w3.org/TR/owl-guide/#OWLGlossary)
- OWL [Reference](http://w3.org/TR/owl-ref/): systematic and compact description
  of OWL
- OWL [Semantics and Abstract Syntax](http://w3.org/TR/owl-semantics/): formal
  and normative definition
- OWL [Test Cases](http://w3.org/TR/owl-test/)
- OWL [Use Cases and Requirements](http://w3.org/TR/webont-req/)

**OWL 2**, for users

- OWL 2 [Overview](http://w3.org/TR/owl-overview/): overview of the documents,
  contains a [list of documents](http://w3.org/TR/owl-overview/#Documentation_Roadmap)
- OWL 2 [Primer](http://w3.org/TR/owl2-primer/)
- OWL 2 [New Features and Rationale](http://w3.org/TR/owl2-new-features/)
- OWL 2 [Quick Reference Guide](http://w3.org/TR/owl2-quick-reference/)

**OWL 2**, specifications

- [Structural Specification and Functional-Style Syntax](http://w3.org/TR/owl2-syntax/)
- [Mapping to RDF Graphs](http://w3.org/TR/owl2-mapping-to-rdf/)
- [Direct Semantics](http://w3.org/TR/owl2-direct-semantics/)
- **(To be continued...)**
- ...

**Misc**

- Entry [Syntax](http://www.w3.org/2007/OWL/wiki/Syntax) on the OWL wiki at W3C.
- Wikipedia [entry](https://en.wikipedia.org/wiki/Web_Ontology_Language).
- Tool to generate a web version of n ontology: [OntoSpec](http://raimond.me.uk/ontospec/).
- As an example of [OntoSpec](http://raimond.me.uk/ontospec/) formatting: the [music ontology](http://musicontology.com/specification/).
- List of tools for [Generating HTML documentation of OWL](http://www.w3.org/2011/prov/wiki/Generating_HTML_documentation_of_OWL), on the PROV Ontology wiki at W3C.

## RIF

Rule Interchange Format.

- The Wikipedia [entry](https://en.wikipedia.org/wiki/Rule_Interchange_Format).

## Linked Data

- W3C [Data Activity](http://w3.org/2013/data/) - *Building the Web of Data*
- [Linked Data Platform Working Group](http://w3.org/2012/ldp/wiki/Main_Page)
- [Linked Data Platform](http://w3.org/TR/ldp/)

## Dynamic Semantic Publishing

- [Sports Refresh: Dynamic Semantic Publishing](http://www.bbc.co.uk/blogs/legacy/bbcinternet/2012/04/sports_dynamic_semantic.html), a BBC blog post, by Jem Rayfield.
- [BBC World Cup 2010 dynamic semantic publishing](http://www.bbc.co.uk/blogs/legacy/bbcinternet/2010/07/bbc_world_cup_2010_dynamic_sem.html), a BBC blog post, by Jem Rayfield.

## Misc

- [Ontotext](http://www.ontotext.com/) website.
- Interesting blog about data and semantics: [datalanguage blog](http://www.datalanguage.com/blog/).
