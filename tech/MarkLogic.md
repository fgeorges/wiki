Some interesting links and notes about MarkLogic.  See also the page
[NoSQL](NoSQL).

## Competitors

This complaint filling, from the State of Oregon against Oracle, is
very interesting: http://www.oregon.gov/docs/082214_filing.pdf.  See
for instance the first paragraph:

> Oracle America, Inc. ("Oracle") fraudulently induced the State of
> Oregon (the "State") and the Oregon Health Insurance Exchange
> Corporation ("Cover Oregon") to enter into contracts for the
> purchase of hundreds of millions of dollars of Oracle products and
> services that failed to perform as promised.  Oracle then repeatedly
> breached those contracts by failing to deliver on its obligations,
> overcharging for poorly trained Oracle personnel to provide
> incompetent work, hiding from the State the true extent of Oracle's
> shoddy performance, continuing to promise what it could not deliver,
> and wilfully refusing to honor its warranty to fix its errors
> without charge.  Over the last three years, Oracle has presented the
> State and Cover Oregon with some $240,280,008 in false claims under
> those contracts.  Oracle’s conduct amounts to a pattern of
> racketeering activity that has cost the State and Cover Oregon
> hundreds of millions of dollars.  Accordingly, plaintiff Ellen
> Rosenblum, the Attorney General for the State of Oregon, along with
> the State and Cover Oregon, brings this lawsuit to recover losses to
> the State and Cover Oregon caused by Oracle’s fraud, racketeering,
> false claims, and broken contracts.

## Libs

- [taskbot](https://github.com/mblakele/taskbot)

## Perfromance

- Good whitepaper on performance: [Understanding System Resources](http://developer.marklogic.com/learn/understanding-system-resources)

## Logs and audit

- [Log Files](https://docs.marklogic.com/guide/admin/logfiles)
- [Auditing Events](https://docs.marklogic.com/guide/admin/auditing)
- [How to use diagnostic trace events](https://help.marklogic.com/Default/knowledgebase/article/View/46/19/how-to-use-diagnostic-trace-events)
- [Diagnostics](http://localhost:8001/diagnostics-admin.xqy) in the admin console (go to Groups > group_name > Diagnostics)

There is no comprehensive list of trace events, but the module
`diagnostics-admin-map.xqy` in a MarkLogic installation contains a
mapping into groups of some of them.  It is de-facto the list of
existing event groups (but not of all existing events).
