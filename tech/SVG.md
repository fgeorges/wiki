Some interesting links and notes about SVG.

- [SVG for technical communication](http://www.svgdocs.net/svgtechcomm.svg).

## D3

D3.js is a JavaScript library for manipulating documents based on
data.  D3 helps you bring data to life using HTML, SVG, and CSS.  D3’s
emphasis on web standards gives you the full capabilities of modern
browsers without tying yourself to a proprietary framework, combining
powerful visualization components and a data-driven approach to DOM
manipulation.

- [Homepage](https://github.com/mbostock/d3)
- [Wiki](https://github.com/mbostock/d3/wiki)
- Examples [gallery](https://github.com/mbostock/d3/wiki/Gallery)
- Other [examples](http://bl.ocks.org/mbostock)
- Sample doc: [Pie Layout](https://github.com/mbostock/d3/wiki/Pie-Layout)
- Video: [D3.js tutorial - 13 - The Pie Layout](http://youtube.com/watch?v=kMCnzUE07QA)

Miscellaneous:

- Article: [Pie and Donut Charts in D3.js](http://schoolofdata.org/2013/10/01/pie-and-donut-charts-in-d3-js/), the theory how to build pie and donut charts using D3, looks simple but very interesting formulas
- Article: [Climbing the d3.js Visualisation Stack](http://schoolofdata.org/2013/08/12/climbing-the-d3-js-visualisation-stack/), about higher-level libraries on top of D3, looks simple but very interesting

### NVD3

This project is an attempt to build re-usable charts and chart
components for d3.js without taking away the power that d3.js gives
you.

The article above about the D3 stack says: "*one of the more mature
libraries, includes line charts, scatterplots (and bubble charts), bar
charts (grouped or stacked), stacked area charts*".

- [NVD3](http://nvd3.org/): Re-usable charts for d3.js
- [NVD3 examples](http://nvd3.org/examples/)
- NVD3 examples: [Pie Chart](http://nvd3.org/examples/pie.html)

### dimple

*Simply Powerful* - The aim of dimple is to open up the power and
flexibility of d3 to analysts.  It aims to give a gentle learning
curve and minimal code to achieve something productive.  It also
exposes the d3 objects so you can pick them up and run to create some
really cool stuff.

- [dimple](http://dimplejs.org/)
- [examples](http://dimplejs.org/examples_index.html)
- [donut example](http://dimplejs.org/examples_viewer.html?id=ring_standard)
