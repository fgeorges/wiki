Notes and links about Git.

- [Escape a git mess](http://justinhileman.info/article/git-pretty/git-pretty.png), step-by-step
- [Git cheatsheet](http://ndpsoftware.com/git-cheatsheet.html#loc=stash;): click
  on an area to see how to go from there to the other areas, and click on a command
  for details below
