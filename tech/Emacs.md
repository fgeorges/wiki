# Emacs

Notes and links about Emacs.  GNU Emacs, of course!

## Basics

**Yanking**

- [browse-kill-ring](https://github.com/browse-kill-ring/browse-kill-ring) allows
  you to, well, browse the kill ring
- so does `helm-show-kill-ring`, part of [Helm](https://github.com/emacs-helm/helm)

## Helm

- home on [Github](https://github.com/emacs-helm/helm)
- a gentle [introduction](http://tuhdo.github.io/helm-intro.html) article
