Some interesting links and notes about data.

See also the page on [Semantics Technologies](Semantics).

## Courses

- Coursera's [Data Science](https://www.coursera.org/specializations/jhudatascience)
  Specialization (9 classes + capstone project)
- Coursera's [Data Analysis and Interpretation](https://www.coursera.org/specializations/data-analysis)
  Specialization (4 classes + capstone project)

## Standards

Some links are on [Semantics Technologies](Semantics), for instance about linked
data.

- Blog post about [Open Data Standards](https://www.linkedin.com/pulse/open-data-standards-steven-adler)
  at W3C.
- W3C [Data Activity](http://w3.org/2013/data/) - *Building the Web of Data*
- Data on the Web Best Practices W3C [working group](http://www.w3.org/2013/dwbp/wiki/Main_Page).
- Data on the Web Best Practices [Use Cases & Requirements](http://www.w3.org/TR/dwbp-ucr/).
- [Data on the Web Best Practices](http://www.w3.org/TR/dwbp/) document.

## Misc

- The Economist's [Graphic detail](http://www.economist.com/blogs/graphicdetail)
  (charts, maps and infographics): a new chart or map every working day,
  interactive-data features and links to interesting sources of data around the
  web.
- NORSE's [Live Attacks](http://map.ipviking.com/), a live map showing network
  attacks as they happen in the world.
- Some nice data-oriented visualizations [over there](http://visual.ly/).
- Open data for [world cup](https://github.com/openfootball/world-cup)
  statistics.  Could be interesting for a demo.
- DBpedia [download page](http://wiki.dbpedia.org/Downloads39) (caution: might
  not be the most up-to-date page...), as a source for triple examples
- Not sure, but keep an eye on the [School of Data](http://schoolofdata.org/)
- [Real Impact Analytics](https://realimpactanalytics.com/en/approach), a Brussels'
  Big Data and Visualization company for telco data.
- A python tutorial on bayesian modeling techniques (PyMC3):
  [Bayesian Modelling in Python https://github.com/markdregan/Bayesian-Modelling-in-Python].

## OECD

- [Better Life Index](http://www.oecdbetterlifeindex.org/countries/belgium/)
- [Regional Well-Being](http://www.oecdregionalwellbeing.org/region.html#BE1)
- [Data Lab](http://www.oecd.org/statistics/datalab/)
- [Statistics](http://www.oecd.org/statistics/)
- [Exchange of Tax Information Portal](http://eoi-tax.org/#default)
