Some interesting links and notes about Javascript.

## Online articles and books

- [Learning JavaScript Design Patterns](http://addyosmani.com/resources/essentialjsdesignpatterns/book/)
- [You Don't Know JS](https://github.com/getify/You-Dont-Know-JS/blob/master/README.md#you-dont-know-js-book-series), book series by [@getify](https://twitter.com/getify), readable online and downloadable as Git!  Look good.

## Javascript and MarkLogic

- [Node.js and Express.js sessions using MarkLogic 8](http://developer.marklogic.com/blog/connect-marklogic), a blog post

## Libraries

Some libraries, in various domains, both for client- and server-side.

### Completion

- Text completion for selection in a select list:
  [Selectize http://selectize.github.io/selectize.js/].  From the website:
  *Selectize is the hybrid of a textbox and <select> box. It's jQuery-based
  and it's useful for tagging, contact lists, country selectors, and so on.*
