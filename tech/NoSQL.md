Some interesting links and notes about NoSQL.  See also the page
[MarkLogic](MarkLogic).

## CAP

- [A plain english introduction to CAP Theorem](http://ksat.me/a-plain-english-introduction-to-cap-theorem/)

## MongoDB

- [Call me maybe: MongoDB stale reads](https://aphyr.com/posts/322-call-me-maybe-mongodb-stale-reads)
