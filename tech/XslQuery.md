Links about the XSLT and XML Query working groups at W3C (defining
XPath, XSLT and XQuery).

### Test suite-related links

* [XQTS](http://dev.w3.org/2006/xquery-test-suite/), the XQuery Test Suite (the [public page](http://dev.w3.org/2006/xquery-test-suite/PublicPagesStagingArea/))
* [QT3 TS](http://dev.w3.org/2011/QT3-test-suite/), the XQuery/XPath/XSLT 3.* Test Suite (ex-''FOTS'')
* QT3 TS's [schema](http://dev.w3.org/2011/QT3-test-suite/catalog-schema.xsd)
* XQuery 3.0 section on [serialization](http://www.w3.org/TR/xquery-30/#id-serialization)
* The [serialization spec](http://www.w3.org/TR/xslt-xquery-serialization-30/) 3.0
* Exselt's [test area](http://exselt.net/w3c/xslt/test-space/)

* Mercurial repo: https://dvcs.w3.org/hg/xslt30-test (at http://dvcs.w3.org/hg)
* Mercurial guide by Anne: http://annevankesteren.nl/2010/08/w3c-mercurial
* Mercurial intro and Subversion re-education: http://hginit.com/

### General links

* [XSL WG Homepage](http://www.w3.org/Style/XSL/) (public)
* [XSL WG Homepage](http://www.w3.org/Style/XSL/Group/) (members only)
* [Query WG Homepage](http://www.w3.org/XML/Query/) (public)
* [Query WG Homepage](http://www.w3.org/XML/Group/Query) (members only)
* [w3c-xsl-wg archives](http://lists.w3.org/Archives/Member/w3c-xsl-wg/)
* [w3c-xsl-query archives](http://lists.w3.org/Archives/Member/w3c-xsl-query/)
* XQuery 1.1 [parser applet](http://www.w3.org/XML/Group/qtspecs/specifications/grammar-11/parser/applets/xquery11/)
* [IRC at W3C](http://www.w3.org/Project/IRC/) (et [commandes de base](http://www.irchelp.org/irchelp/misc/frnew2irc.html))
* [Zakim instructions](http://www.w3.org/2002/01/UsingZakim), the teleconference tool at W3C
* [XSLTS 1.1.0](http://www.w3.org/XML/Group/xslt20-test/PublishedTestSuites/XSLTS_1_1_0.zip), the latest known published XSLT test suite (members only)
* [Issue tracking](http://www.w3.org/Bugs/) systems (aka Bugzilla): [public](http://www.w3.org/Bugs/Public/) instance and [member-only](http://www.w3.org/Bugs/Member/) instance

### Editor drafts

3.0

* [XPath 3.0](http://www.w3.org/XML/Group/qtspecs/specifications/xquery-30/html/xpath-30.html) editor draft
* [XSLT 3.0](http://www.w3.org/XML/Group/qtspecs/specifications/xslt-30/html/Overview-diff.html) editor draft
* [XQuery 3.0](http://www.w3.org/XML/Group/qtspecs/specifications/xquery-30/html/xquery-30.html) editor draft
* [F&O 3.0](http://www.w3.org/XML/Group/qtspecs/specifications/xpath-functions-30/html/Overview.html) editor draft
* [XDM 3.0](http://www.w3.org/XML/Group/qtspecs/specifications/xpath-datamodel-30/html/Overview.html) editor draft
* [XSLT 3.0 Use Cases](http://www.w3.org/XML/Group/2007/xsl-streaming/usecases.html) editor draft
* [XSLT 3.0 XSD schema](http://www.w3.org/2010/schema-for-xslt30.xsd) draft

3.1

* [XPath 3.1](http://www.w3.org/XML/Group/qtspecs/specifications/xquery-31/html/xpath-31.html) editor draft
* [XQuery 3.1](http://www.w3.org/XML/Group/qtspecs/specifications/xquery-31/html/xquery-31.html) editor draft
* [F&O 3.1](http://www.w3.org/XML/Group/qtspecs/specifications/xpath-functions-31/html/Overview.html) editor draft

### Old drafts, for 1.1/2.1

* [XSLT 2.1](http://www.w3.org/XML/Group/qtspecs/specifications/xslt-21/html/Overview-diff.html) editor draft
* [XPath 2.1](http://www.w3.org/XML/Group/qtspecs/specifications/xquery-11/html/xpath-21.html) editor draft
* [F&O 1.1](http://www.w3.org/XML/Group/qtspecs/specifications/xpath-functions-11/html/Overview-diff.html) editor draft
* [XQuery 1.1](http://www.w3.org/XML/Group/qtspecs/specifications/xquery-11/html/xquery-11.html) editor draft
* [XSLT 2.1 Use Cases](http://www.w3.org/XML/Group/2007/xsl-streaming/usecases.html) editor draft
* [XSLT 2.1 XSD schema](http://www.w3.org/2010/schema-for-xslt21.xsd) draft

### Latest working drafts (and CR, REC, etc.)

* [XSL Transformations (XSLT) Version 3.0](http://www.w3.org/TR/xslt-30/) working draft
* [XSLT 2.0, Second Edition](http://www.w3.org/TR/2009/PER-xslt20-20090421/), proposed edited recommendation
* [XQuery 3.0 Requirements](http://www.w3.org/TR/xquery-30-requirements/) working draft
* [XQuery Scripting Extension 1.0](http://www.w3.org/TR/xquery-sx-10/) working draft

### Recommendations

XPath

* [XPath 1.0](http://www.w3.org/TR/xpath/)
* [XPath 2.0](http://www.w3.org/TR/xpath20/)
* [XPath 3.0](http://www.w3.org/TR/xpath-30/)

XQuery

* [XQuery 1.0](http://www.w3.org/TR/xquery/)
* [XQuery 3.0](http://www.w3.org/TR/xquery-30/)
* [XQueryX 1.0](http://www.w3.org/TR/xqueryx/)
* [XQueryX 3.0](http://www.w3.org/TR/xqueryx-30/)
* [XQuery Update 1.0](http://www.w3.org/TR/xquery-update-10/)

XSLT

* [XSLT 1.0](http://www.w3.org/TR/xslt/)
* [XSLT 2.0](http://www.w3.org/TR/xslt20/)
* [XSLT Requirements 2.0](http://www.w3.org/TR/xslt20req)

Common RECs

* [Serialization 1.0](http://www.w3.org/TR/xslt-xquery-serialization/)
* [Serialization 3.0](http://www.w3.org/TR/xslt-xquery-serialization-30/)
* [XDM 1.0](http://www.w3.org/TR/xpath-datamodel/)
* [XDM 3.0](http://www.w3.org/TR/xpath-datamodel-30/)
* [F&O 1.0](http://www.w3.org/TR/xpath-functions/)
* [F&O 3.0](http://www.w3.org/TR/xpath-functions-30/)
* [Formal Semantics 1.0](http://www.w3.org/TR/xquery-semantics/)
* [Full Text 1.0](http://www.w3.org/TR/xpath-full-text-10/)

### Meetings

* Prague F2F March 2010's [minutes](http://www.w3.org/XML/Group/2010/03/xsl-minutes.xml).
* Oxford joint F2F July 2010's [info page](http://www.w3.org/XML/Group/2010/06/oxford_f2f.html) and [minutes](http://www.w3.org/XML/Group/2010/07/xsl-ftf-minutes.xml).
* TPAC 2011 in Santa Clara's [info page](http://www.w3.org/2011/11/TPAC/)
* Next [XML meetings](http://www.w3.org/XML/Group/Overview.html)
* Calendar of [member events](http://www.w3.org/Member/Eventscal)
* All [events](http://www.w3.org/participate/eventscal.html)

### Various Specs

* [XProc](http://www.w3.org/TR/xproc/)
* [XML Schema 1: Structures](http://www.w3.org/TR/xmlschema-1/)
* [XML Schema 2: Datatypes](http://www.w3.org/TR/xmlschema-2/)

### Notes

* Unicode regexs... (testing encoding for serialization in XQuery test suite): http://www.w3.org/International/questions/qa-forms-utf-8 (given by Jim Fuller)
