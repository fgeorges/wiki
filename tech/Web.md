# Web

Page about web development, tools and frameworks.

- Article: [How to use Chrome DevTools like a Pro](http://medium.com/jotform-form-builder/b9bd414870e3)

## Gallery tools

Tools, frameworks and libraries to write photo (and video?) gallery websites,

- [Galleria](http://galleria.io/) (last version, 1.4.2, on 2014-08-11)
- [PhotoSwipe](http://photoswipe.com/)

Galleria seems to support video, not PhotoSwipe.  For the former, the example
gallery on its homepage contains one video from Vimeo at least (is it possible
to play own MP4 files from the server?)  For the latter, this Github
[issue](https://github.com/dimsemenov/PhotoSwipe/issues/651) suggests it does
not support video yet, but will at some point.

For pure video, Video.js is the one mentioned everywhere:

- [Video.js](http://videojs.com/), *a free and open source HTML5 video player*
