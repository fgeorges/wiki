Various tools for tech and dev.

## Screencast

Very clear, concise, and simple explanations about
[OS X Screencast to animated GIF](https://gist.github.com/dergachev/4627207)
(that is, how to capture a screencast, and create an **animated GIF** out of it).
Very, very useful to report issues, bugs, etc.  Well, to communicate about a
piece of software.

Basically, use Quicktime Player to capture the screencast, and the following
command to convert the `*.mov` file to an animated GIF (`ffmpeg` and `gifsicle`
are available through `brew install`):

```
ffmpeg -i in.mov -s 600x400 -pix_fmt rgb24 -r 10 -f gif - \
    | gifsicle --optimize=3 --delay=3 > out.gif
```