Some pages with useful commands:

- [The Art of Command Line](https://github.com/jlevy/the-art-of-command-line/blob/master/README.md) is quite concise, with some useful info, to read from top to bottom
- the [Unix Toolbox](http://cb.vu/unixtoolbox.xhtml) is a very useful page, with a lot of complete commands to perform specific tasks, well organized (see also its [about](http://cb.vu/unixtoolbox.xhtml) page).
- the [Rosetta Stone for Unix](http://bhami.com/rosetta.html), as its name suggests, give commands in several flavours (Linux, FreeBSD, Solaris, Mac OS X, etc.)
- [Command Line Fu](http://www.commandlinefu.com/) is a website I've seen mentioned a few times; I don't really like it, but it can be useful sometimes when looking for a precise solution, I guess

Disk tips:

- use [ncdu](http://dev.yorhel.nl/ncdu/scr) (the NCurses Disk Utility) to interactively browse through your FS and see where space is most used, using a full-screen NCurses interface working great through SSH, a **must-have**!
