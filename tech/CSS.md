Infos and links about CSS.

## General

- Mozilla's [CSS Getting Started](https://developer.mozilla.org/en-US/docs/Web/Guide/CSS/Getting_Started) tutorial
- Articles at [A List Apart](http://alistapart.com/articles), a very interesting and long-running magazine on web technologies, CSS and Javascript
- article: [CSS Animation for Beginners](https://robots.thoughtbot.com/css-animation-for-beginners)
- article: [CSS Transitions and Transforms for Beginners](https://robots.thoughtbot.com/transitions-and-transforms)

Good article on Best Practices and Naming Conventions:
[CSS Architecture for Design Systems](http://bradfrost.com/blog/post/css-architecture-for-design-systems/).

## Box model

- Mozilla's [Box model](https://developer.mozilla.org/en-US/docs/Web/CSS/box_model) article
- article: [Opening the Box Model](http://learn.shayhowe.com/html-css/opening-the-box-model/) (looks very good!)
- article: [The CSS Box Model](https://css-tricks.com/the-css-box-model/) (seems concise but yet complete)
- article: [The Box Model For Beginners](https://www.addedbytes.com/articles/for-beginners/the-box-model-for-beginners/) (seems concise but yet complete)

## Frameworks

- Zurb's [Foundation](http://foundation.zurb.com/sites/getting-started.html), the alternative to Bootstrap Matt Patterson told me about

### Bootstrap

- some [tutorial](http://ieatcss.com/twitter-bootstrap-tutorial.html) (not sure whether it is good or not)
- some [tutorial](http://www.tutorialrepublic.com/twitter-bootstrap-tutorial/) (probably too simplistic, but might have interesting simple examples)
- article: [Exploring the Bootstrap 3.0 Grid System](http://fearlessflyer.com/exploring-the-bootstrap-3-0-grid-system/) (seems concise but yet complete)

Books:

- [Jump Start Bootstrap](https://learnable.com/books/jump-start-bootstrap) - 160 pp. - looks excellent!
- [Learning Bootstrap](http://www.amazon.fr/dp/B00RP13B5S/) - 180 pp.
- [Extending Bootstrap](http://www.amazon.fr/dp/B00J5KHLA8/) - 70 pp.

Tools:

- [Free themes for Bootstrap](http://bootswatch.com/)
- [Theme example](http://getbootstrap.com/examples/theme/)
- [Bootstrap Magic](http://pikock.github.io/bootstrap-magic/app/index.html), an online Bootstrap theme editor

### Semantic UI

- Official [homepage](http://semantic-ui.com/)
- Official [Learn Semantic](http://learnsemantic.com/) website
