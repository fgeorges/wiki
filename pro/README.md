Wiki pages for profesional interests.

## Misc

-  A Beginner's Guide to [EU Funding](http://ec.europa.eu/budget/funding)

## Admin

- [SPF Economie](http://economie.fgov.be/fr/)
- [Recherche BCE](http://kbopub.economie.fgov.be/kbopub/zoekwoordenform.html?lang=fr) (aussi pour rechercher les publications, depots de bilan, etc.)
